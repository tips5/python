#!/usr/bin/env python3.10
# -*- coding: utf-8 -*-

# TBD
def unpack_func(*args):
    return (arg for arg in args if type(arg) is str)


if __name__ == '__main__':
    print(*(i for i in unpack_func(*[1, 2, 'a', 'b'])))
